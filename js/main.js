// renderList với method tương ứng với từng đối tượng

let vitri =null
let renderListWithMethod = (ListPerson) => {
    renderList(ListPerson.map((item) => {
        if (item.type == "Sinh Viên") {
            return {
                ...item,
                calculateDTB : function() {
                   
                    return ((Number(item.math)+Number(item.physical)+Number(item.chemistry))/3).toFixed(2)
                }
            }

        }
        else if (item.type == "Giáo Viên") {
            return {
                ...item,
                calculateLuong : function() {
                 
                    return (item.date*item.luongtheongay)
                }
            }

        }
        else if (item.type == "Khách Hàng") {
            return {
                ...item,
            }
        }
        }))
      
       
    

}
let PersonelManager1 = new PersonelManager()
let showData = localStorage.getItem("DATA")
if (showData!==null) {
    PersonelManager1.ListPerson =  JSON.parse(showData)
    renderListWithMethod(PersonelManager1.ListPerson)
    
} 
let addPerson = ()=> {
    DomID('ma').disabled = false
    document.getElementById("SinhVien").style.display ="none"
    document.getElementById("GiaoVien").style.display ="none"
    document.getElementById("KhachHang").style.display ="none"
    $('#exampleModal').modal('show')
    document.querySelector(".them").style.display = "inline"
    document.querySelector(".capNhat").style.display = "none"
    document.querySelector('form').reset()
  
   
}
let selectOptions = () =>{
    let type = DomID("type").value
    switch(type) {
        case "Sinh Viên" :
            DomID('SinhVien').style.display = "block"
            DomID('GiaoVien').style.display = "none"
            DomID("KhachHang").style.display = "none"
            break;
        case "Giáo Viên" : 
        DomID('GiaoVien').style.display = "block"
            DomID('SinhVien').style.display = "none"
            DomID("KhachHang").style.display = "none"
            break;
        case "Khách Hàng" :
            DomID('KhachHang').style.display = "block"
            DomID('SinhVien').style.display = "none"
            DomID("GiaoVien").style.display = "none"
            break;
        default :
        DomID('SinhVien').style.display = "none"
        DomID("GiaoVien").style.display = "none"
        DomID('KhachHang').style.display = "none"
        break;
    }


}
let Them= () => {
    let person = layThongTinTuForm()
    if (person==undefined) {
      person =  new Person ('','','','','','')
    }  
    if (isValidThem(person.type)) {
 
            document.getElementById("type").value = "none"
            document.getElementById("LoaiNguoiDung").value = "Mặc Định"
            PersonelManager1.addPerson(person)
            renderListWithMethod(PersonelManager1.ListPerson)
            let data = JSON.stringify(PersonelManager1.ListPerson)
            localStorage.setItem("DATA",data)
            $('#exampleModal').modal('hide')
    }

  
} 
let Xoa = (ma) =>{
    PersonelManager1.removePeron(ma)
    renderListWithMethod(PersonelManager1.ListPerson)
    let data = JSON.stringify(PersonelManager1.ListPerson)
    localStorage.setItem("DATA",data)
}
let Sua = (ma) => {
    let arrThongBao = document.querySelectorAll('.sp-thongBao')
    for (let i = 0;i<arrThongBao.length;i++) {
        arrThongBao[i].innerHTML = ''
    }
    DomID('ma').disabled = true
    let index = PersonelManager1.updatePerson(ma)
    vitri = index
    document.querySelector(".them").style.display = "none"
    document.querySelector(".capNhat").style.display = "inline"
    $('#exampleModal').modal('show')
    showThongTinlenForm(PersonelManager1.ListPerson[index])
}
let Update = () => {
    let person =layThongTinTuForm()
    if (person==undefined) {
        person =  new Person ('','','','','','')
      }  
    if (isValidUpDate(person.type)) {
        PersonelManager1.ListPerson[vitri] = person
        renderListWithMethod(PersonelManager1.ListPerson)
        let data = JSON.stringify(PersonelManager1.ListPerson)
        localStorage.setItem("DATA",data)
        $('#exampleModal').modal('hide')   
    }
}
let selectNguoiDung =() => {
    let listPerson = PersonelManager1.ListPerson
    let nguoiDung = document.getElementById("LoaiNguoiDung").value
    let listType  = listPerson.filter((person) => person.type==nguoiDung)
    if (nguoiDung=="Mặc Định") {
        renderListWithMethod(listPerson)    
        }
    else{
        renderListWithMethod(listType)
    }
}

let sapXepTheoHoTen = () => {
    let listName = PersonelManager1.ListPerson  
        let listSortName = listName.sort((nguoiTiepTheo,nguoi) => {
            let tenNguoiTiepTheo = (nguoiTiepTheo.hoTen.toLowerCase()).split(' ')
            let tenNguoi = ((nguoi.hoTen).toLocaleLowerCase()).split(' ')
            if (removeAccents(tenNguoiTiepTheo[tenNguoiTiepTheo.length-1])>removeAccents(tenNguoi[tenNguoi.length-1])) {
                return 1
            }
            else if (removeAccents(tenNguoiTiepTheo[tenNguoiTiepTheo.length-1])<removeAccents(tenNguoi[tenNguoi.length-1])) {
                return -1
    
            }
            return 1
         }) 
         renderListWithMethod(listSortName)
         let data = JSON.stringify(PersonelManager1.ListPerson)
         localStorage.setItem("DATA",data)
}
// hàm loại bỏ các dấu trong tiếng việt về thành tiếng anh 
let removeAccents = (str) => {
    let AccentsMap = [
      "aàảãáạăằẳẵắặâầẩẫấậ",
      "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
      "dđ", "DĐ",
      "eèẻẽéẹêềểễếệ",
      "EÈẺẼÉẸÊỀỂỄẾỆ",
      "iìỉĩíị",
      "IÌỈĨÍỊ",
      "oòỏõóọôồổỗốộơờởỡớợ",
      "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
      "uùủũúụưừửữứự",
      "UÙỦŨÚỤƯỪỬỮỨỰ",
      "yỳỷỹýỵ",
      "YỲỶỸÝỴ"    
    ];
    for (let i=0; i<AccentsMap.length; i++) {
      let re = new RegExp('[' + AccentsMap[i].substring(1) + ']', 'g');
      let char = AccentsMap[i][0];
      str = str.replace(re, char);
    }
    return str;
  }
